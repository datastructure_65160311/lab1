import java.util.Scanner;

public class ArrayManipulation {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int numbers[] = { 5, 8, 3, 2, 7,9 };
        String names[] = { "Alice", "Bob", "Charlie", "David" };
        double values[] = new double[4];

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < values.length; i++) {
            values[i] = kb.nextDouble();
        }

        int sumNumbers = 0;
        for (int i = 0; i < numbers.length; i++) {
            sumNumbers += numbers[i];
        }
        System.out.println("sum of all numbers = " + sumNumbers);

        double max = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("max values = " + max);

        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - (i + 1)];
        }

        for (int i = 0; i < names.length; i++) {
            System.out.print(reversedNames[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                int temp = 0;
                if (numbers[j] < numbers[i]) {
                    temp = numbers[i];
                    numbers[i] = numbers[j];
                    numbers[j] = temp;
                }
            }
            System.out.print(numbers[i] + " ");
        }

    }

}